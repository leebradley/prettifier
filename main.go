//go:generate go run -tags generate gen.go

package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"

	"github.com/go-xmlfmt/xmlfmt"
	"github.com/zserge/lorca"
)

func main() {

	ui, err := lorca.New("", "", 800, 620)
	if err != nil {
		log.Printf("err: %v", err)
	}

	defer ui.Close()

	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		log.Fatal(err)
	}
	defer ln.Close()

	go http.Serve(ln, http.FileServer(FS))
	ui.Load(fmt.Sprintf("http://%s", ln.Addr()))

	ui.Bind("prettify", pretty)

	// ui.Bind("start", func() {
	// 	log.Println("UI is ready")
	// })

	// ui.Bind("add", func(a, b int) int {
	// 	return a + b
	// })

	// Calls Js to increment value in HTML every second
	// go func() {
	// 	for i := 0; ; i++ {
	// 		ui.Eval(fmt.Sprintf("document.getElementById('span1').innerHTML = %d;", i))
	// 		time.Sleep(1 * time.Second)
	// 	}
	// }()

	// Wait until the interrupt signal arrives or browser window is closed
	sigc := make(chan os.Signal)
	signal.Notify(sigc, os.Interrupt)
	select {
	case <-sigc:
	case <-ui.Done():
	}

	log.Println("exiting...")
}

func pretty(val string) string {
	val = strings.TrimSpace(val)

	if len(val) == 0 {
		return "please enter something to format"
	}

	//	log.Printf("received: %s", val)

	bytes := []byte(val)

	//check json
	if json.Valid(bytes) {
		pretty, err := FormatJSON(strings.NewReader(val))
		if err != nil {
			return err.Error()
		}

		return fmt.Sprintf("%s", pretty)
	}

	//check xml
	if bytes[0] == '<' {
		// this looks like the ticket for prettifying xml:
		// https://github.com/go-xmlfmt/xmlfmt/
		return xmlfmt.FormatXML(val, "", "  ")
	}

	//try base64 decoding
	v, err := base64.StdEncoding.DecodeString(val)
	if err == nil {
		// it worked! prettify result
		return pretty(string(v))
	}

	return "dunno..."

}

//FormatJSON prettifies unformatted valid json. If the supplied json is
//not valid, an error is returned.
func FormatJSON(ugly io.Reader) ([]byte, error) {
	var p interface{}
	err := json.NewDecoder(ugly).Decode(&p)
	if err != nil {
		return nil, err
	}

	return json.MarshalIndent(p, "", "  ")
}
