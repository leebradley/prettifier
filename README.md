# prettifier

This project provides a command line json and xml prettifier


## development

To build and run in one step:

```
go generate; go build; ./prettifier
```